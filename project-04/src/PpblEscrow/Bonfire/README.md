## Testing with trace 

* run `cabal update` in /project-04
* run `cabal build`
* run `cabal repl`
* run `import PpblEscrow.Bonfire.Trace`
* now you can run some tests 
- mintTest will mint a token 
- startTest will lock a token at the escrow contract 
- cancelTest will get you the token back 

