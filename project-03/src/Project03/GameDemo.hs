{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Project03.GameDemo where

import Ledger hiding (singleton)
import Ledger.Typed.Scripts
import qualified PlutusTx
import PlutusTx.Prelude hiding (Semigroup (..), unless)
import Prelude (Show (..))

-- 3.1.5
-- Video lecture: "What can a UTxO hold?"
-- Context can show that a UTxO holds tokens. Let's pick up where we left off in Week 3 by revisiting TxInfo
-- With ScriptContext, our Validator can:
-- check that a certain player is playing
-- check that a transaction is happening within certain time boundaries
-- check whether a participant holds a certain token (We will need to build transactions that can prove this)

-- A UTxO can hold Datum (see ccli). Let's take a first look at how Datum works in a Smart Contract
-- In a guessing game, Datum would hold the "answer"

-- Redeemer can pass information to the Validator: here is how it's often used --> ACTIONS in the context of a Game
-- In a guessing game, we might have a "Host" and a "Guesser". We will need ways for both of them to interact withe the contract.
-- The Redeemer is where a participant can identify themselves to the contract.
-- Particpants can state the Action they'd like to take
-- Hosts can start or end a contest
-- Guessers can try to guess a number (essentially hacking our contract...like we saw in 3.1.2)

data GameAction = Guess | EndGame
  deriving (Show)

PlutusTx.makeIsDataIndexed ''GameAction [('Guess, 0), ('EndGame, 1)]
PlutusTx.makeLift ''GameAction

-- start with a bad implementation, then live code this week

{-# INLINEABLE mkValidator #-}
mkValidator :: Integer -> GameAction -> ScriptContext -> Bool
mkValidator answer action ctx =
  case action of
    Guess ->
      traceIfFalse "Your guess is wrong!" checkGuess
        && traceIfFalse "You need a game play token" checkHasPlayToken
        && traceIfFalse "You must send at least 5 tAda to the contract" checkIsGambling
    EndGame -> traceIfFalse "You are not the host of this Guess" checkIsHost
  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    checkGuess :: Bool
    checkGuess = answer == 314159
    -- datum must match datum hash: but this is already built into Plutus!
    -- Version 1: datum must match 314159
    -- Version 2: datum must simply BE any Integer

    checkHasPlayToken :: Bool
    checkHasPlayToken = True
    -- one way to let someone play is with a token, but we're not going to implement it quite yet.
    -- first, let's just see if the player is ready to gamble
    -- maybe live coding on Friday? or just Week 5...

    checkIsGambling :: Bool
    checkIsGambling = True
    -- we just want to know if the player is submitting money to the contract
    -- but there's a problem. the contract will only allow them to play if they're going to win in the first place
    -- this game can only lose us money
    -- so we have to think a bit more about it: join us for live coding this week! (Maybe Thursday)

    checkIsHost :: Bool
    checkIsHost = True

-- How does a contract address know where a UTxO came from??
-- We can store it in Datum.
-- This will require a special new data type...

data Typed

instance ValidatorTypes Typed where
  type DatumType Typed = Integer
  type RedeemerType Typed = GameAction

typedValidator :: TypedValidator Typed
typedValidator =
  mkTypedValidator @Typed
    $$(PlutusTx.compile [||mkValidator||])
    $$(PlutusTx.compile [||wrap||])
  where
    wrap = wrapValidator @Integer @GameAction

validator :: Validator
validator = validatorScript typedValidator
