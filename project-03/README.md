# Plutus Project Based Learning
## Course #1 | Gimbalabs | February - April 2022

## Project #3: Build a Game
- Examples of how to use Context: which tokens, how many? who is the player?
- Examples Redeemer: What "move" is the player making?
- Examples Datum: "Holding game state".

## README:

### 3.1.1 What can a Validator do?
1. It Validates a transaction, taking Datum, Redeemer, and Context into account
2. Just like in Project 1, let's compile `my-first-script.plutus`
3. A compiled Validator script can be used to create a Contract address. Here is how:
```
cd /project-03/output
cardano-cli address build --payment-script-file my-first-script.plutus --testnet-magic 1097911063 --out-file my-first-script.addr
```
4. Let's take a look at that new address
```
cardano-cli query utxo --testnet-magic 1097911063 --address addr_test1wpnlxv2xv9a9ucvnvzqakwepzl9ltx7jzgm53av2e9ncv4sysemm8
```
What is going on here?

Notice TxOutDatumHash and TxOutDatumNone.

Contract Addresses are different from Wallet Addresses, even though they look the same. "Sending to" and "unlocking from" require different sorts of transactions.

## 3.1.2 Hacking the "Always Succeeds" Contract
1. We can send funds to a Contract Address. First we need a Datum Hash. So what are hashing?
ccli examples

### Set Variables
```
SENDER
SENDERKEY
CONTRACTADDR
TXIN
DATUMHASH
```

```
cardano-cli transaction build \
--alonzo-era \
--tx-in $TXIN \
--tx-out $CONTRACTADDR+4000000 \
--tx-out-datum-hash $DATUMHASH \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file tx.raw \
--testnet-magic 1097911063

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file tx.raw \
--out-file tx.signed

cardano-cli transaction submit \
--tx-file tx.signed \
--testnet-magic 1097911063
```
2. We can unlock funds at a Contract Address
```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-in-script-file validator-example.plutus \
--tx-in-datum-value [whatever we hashed] \
--tx-in-redeemer-value [is actually irrelevant here, but must be included] \
--tx-in-collateral $COLLATERAL \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```

3. So: What would we need in order to unlock someone else's transaction?
Let's take a closer look at that contract addresss:
```
```

Then, let's see if we can guess what datum was hashed:
```
cardano-cli transaction hash-script-data --script-data-value 0
> 03170a2e7597b7b7e3d84c05391d139a62b157e78786d8c082f29dcf4c111314
```

EVEN BETTER
```
cardano-cli transaction hash-script-data --script-data-file datum.json
> 923918e403bf43c34b4ef6b48eb2ee04babed17320d8d1b9ff9ad086e86f44ec
```

where `datum.json` is
```
{"constructor":0,"fields":[]}
```
(But more on this later...!)

Example of unlocking
```
SENDER=
SENDERKEY=
TXIN=
COLLATERAL=
```

WHAT IS HAPPENING HERE!? (Make note of everything this transaction DOES NOT have.)

```
cardano-cli transaction build \
--alonzo-era \
--testnet-magic 1097911063 \
--tx-in $TXIN \
--tx-in-script-file my-first-script.plutus \
--tx-in-datum-file datum.json \
--tx-in-redeemer-value 42 \
--tx-in-collateral $COLLATERAL \
--change-address $SENDER \
--protocol-params-file protocol.json \
--out-file unlock.raw

cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--testnet-magic 1097911063 \
--tx-body-file unlock.raw \
--out-file unlock.signed

cardano-cli transaction submit \
--tx-file unlock.signed \
--testnet-magic 1097911063
```
https://testnet.cardanoscan.io/transaction/5072a358d37cfa96847772b94a465abd88fb5638dbfc005e9705789ea7653e33?tab=utxo

## 3.1.3 and 3.1.4 - In Front End
[https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01-front-end](https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01-front-end)

## 3.1.5
[https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01/-/blob/master/project-03/project315.md](https://gitlab.com/gimbalabs/ppbl-course-01/ppbl-course-01/-/blob/master/project-03/project315.md)